import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * При помощи программы на Java с использованием java.net.URLConnection запросить с сайта страницу по указанному URL.
 * URL страницы должен указываться пользателем. Тело страницы сохранить в файл.
 * Заголовки, отданные сервером, вывести в консоль.
 */
public class URLbyURLConnection {
    private PrintWriter output;
    private BufferedReader input;
    private URLConnection urlConnection;

    public void start(String url) throws IOException {
        urlConnection = new URL(url).openConnection();
        output = new PrintWriter(new FileOutputStream("failURLConn.txt"), true);
        input = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
    }

    public void send() throws IOException {
        String lineInput;
        while ((lineInput = input.readLine()) != null) {
            if (".".equals(lineInput)) {
                break;
            }
            output.println(lineInput);
        }
    }

    public void print(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();
        Map<String, List<String>> headerFields = urlConnection.getHeaderFields();
        System.out.println(headerFields.toString());
        urlConnection.disconnect();
    }

    public void stop() throws IOException {
        input.close();
        output.close();
    }

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter url: ");
        String url = scanner.next();
        URLbyURLConnection urlConnection = new URLbyURLConnection();
        urlConnection.start(url);
        urlConnection.send();
        urlConnection.print(new URL(url));
        urlConnection.stop();
    }
}
