import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * При помощи программы на Java с использованием java.net.Socket запросить с сайта страницу по указанному URL.
 * URL страницы должен указываться пользателем. Тело страницы сохранить в файл.
 * Заголовки, отданные сервером, вывести в консоль.
 */
public class URLbySocket {
    public void send(String[] hostAndResource) {
        String host = hostAndResource[0];
        String resource = "/";
        if (hostAndResource.length == 2)
            resource += hostAndResource[1];

        try (Socket socket = new Socket(host, 80)) {
            socket.getOutputStream().write((
                    "GET " + resource + " HTTP/1.1\n" +
                            "Host: " + host + ":80\n" +
                            "User-Agent: Chrome (Ubuntu; Intel Linux OS X 20.4) " +
                            "AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15\n\n"
            ).getBytes());

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                System.out.println("Headers:");
                String line;
                while (!(line = reader.readLine()).isEmpty()) {
                    System.out.println(line);
                }

                try (PrintWriter writer = new PrintWriter(new FileOutputStream("fileSocket.txt"), true)) {
                    String lineInput;
                    while ((lineInput = reader.readLine()) != null) {
                        if (".".equals(lineInput)) {
                            break;
                        }
                        writer.println(lineInput);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        URLbySocket socketClient = new URLbySocket();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the address");
        String[] hostAndResource = scanner.next().split("/", 2);

        socketClient.send(hostAndResource);
    }
}
